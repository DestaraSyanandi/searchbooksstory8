$(document).ready(function(){
    $("button").click(
      function(){
        var judulBuku = $("#searchField").val()
        console.log("start", judulBuku)
        $.ajax({
          method: "GET",
          url: "https://www.googleapis.com/books/v1/volumes?q="+judulBuku,
          success: function(result){
            $("table").empty();
            $("table").append(
              '<thead>' +
                '<tr>' +
                  '<th scope="col">Gambar Cover</th>' +
                  '<th scope="col">Judul Buku</th>' +
                  '<th scope="col">Penulis</th>' +
                  '<th scope="col">Deskripsi/Sinopsis</th>' +
                  '<th scope="col">Tanggal Publish</th>' +
                '</tr>'+
              '</thead>'
            );
            for(i = 0; i < result.items.length ; i++){
              $("table").append(
                "<tbody>" +
                  "<tr>" +
                    "<th scope='row'>" + "<img src= '"+ result.items[i].volumeInfo.imageLinks.thumbnail + "'>" + "</td>" +
                    "<td>" + result.items[i].volumeInfo.title + "</td>" +
                    "<td>" + result.items[i].volumeInfo.authors + "</td>" +
                    "<td>" + result.items[i].volumeInfo.description + "</td>" +
                    "<td>" + result.items[i].volumeInfo.publishedDate + "</td>" +
                  "</tr>" +
                "</tbody>"
              );
            }
          }
        })
      }
    );
  })
  
  $("input").change(
    function(){
      console.log("start start")
    }
  );
  
